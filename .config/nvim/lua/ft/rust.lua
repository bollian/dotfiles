local options = require 'options'

options.merge_into(vim.opt, {
  wrap = false,
})
